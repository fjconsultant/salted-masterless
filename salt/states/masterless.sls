/srv/salt/masterless:
  file.directory:
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

masterless:
  git.latest:
    - name: https://fjconsultant@bitbucket.org/fjconsultant/salted-masterless.git
    - target: /srv/salt/masterless
    - force: True

