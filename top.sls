masterless:
  '*':
    - salt.states.base
    - salt.states.masterless
    - salt.minion
